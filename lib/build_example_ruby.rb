require_relative 'build_example_ruby/version'
require 'sequel'
require 'bunny'
require 'pg'
require 'json'
require 'config'
# This is a test project.
module BuildExampleRuby
  class Base
    attr_accessor :base

    def initialize
      @base = JSON.parse('{}')
      configure
      puts 'initialized'
    end

    def configure
      if ENV['APP_ENV']
        case ENV['APP_ENV']
          when 'TEST'
            Config.load_and_set_settings(File.dirname(__FILE__) + '../../config/environments/testing.yml')
          when 'DEV'
            Config.load_and_set_settings(File.dirname(__FILE__) + '../../config/environments/development.yml')
          when 'PROD'
            Config.load_and_set_settings(File.dirname(__FILE__) + '../../config/environments/production.yml')
          else
            Config.load_and_set_settings(File.dirname(__FILE__) + '../../config/settings.yml')
        end
      else
        Config.load_and_set_settings(File.dirname(__FILE__) + '../../config/settings.yml')
      end
    end

    def run
      ##Test
      puts 'Listening for requests...'
      loop do
        sleep(30)
      rescue Interrupt => _
        return
      end
    end

    # def test
    #   puts 'Test Uncovered Code'
    # end
  end
end
