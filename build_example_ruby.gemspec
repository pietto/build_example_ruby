
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'build_example_ruby/version'

Gem::Specification.new do |spec|
  spec.name          = 'build_example_ruby'
  spec.version       = BuildExampleRuby::VERSION
  spec.authors       = ['paul.ferguson']
  spec.email         = ['paul.ferguson@metova.com']

  spec.summary       = %q{Ruby/Jenkins/Ansible/LXC Example}
  spec.description   = %q{Example of Building Ruby Package for Jenkins/Ansible/LXC}
  spec.homepage      = 'http://localhost'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = 'localhost'
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'simplecov', '~> 0.16'
  spec.add_development_dependency 'simplecov-cobertura', '~> 1.3.0', '>=1.3.0'
  spec.add_development_dependency 'gem-compiler', '~> 0.8'

  spec.add_runtime_dependency 'bunny', '~> 2.9'
  spec.add_runtime_dependency 'config', '~>1.7'
  spec.add_runtime_dependency 'pg', '~> 1.0'
  spec.add_runtime_dependency 'sequel', '~> 5.5'
end
