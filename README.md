# BuildExampleRuby [![Codacy Badge](https://api.codacy.com/project/badge/Grade/14aa54d79114476ea4caba356d055bc9)](https://www.codacy.com?utm_source=bitbucket.org&amp;utm_medium=referral&amp;utm_content=cybercents/build_example_ruby&amp;utm_campaign=Badge_Grade)

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'build_example_ruby'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install build_example_ruby

## Usage

  This is an Example of a Git Repo configured to be built on Jenkins, in an LXC container, using Ansible Playbooks.

## Development

  This is a test commit
