#!/usr/bin/env groovy
import hudson.tasks.test.AbstractTestResultAction

pipeline {
    agent { label 'slave-agent-1' }

    environment {
        PROJECT_NAME = get_project_name()
    }

    stages{
        stage('Build') {
            steps {
                sh label: 'Debug: Print env vars', script: '''printenv | sort'''
                sh label: 'Installing bundle dependencies.', returnStdout: true, script: '''bundle install'''
                sh label: 'Packaging bundle dependencies.', returnStdout: true, script: '''bundle package'''
            }
        }

        stage('Unit Test') {
            steps {
                sh label: 'Running tests.', script: '''bundle exec rspec'''
            }
        }

        stage('Package') {
            steps {
                create_project_tar()
            }
        }

        stage('Deploy to development environment.') {
            when { not { anyOf { branch 'feature/release*'; branch 'bugfix/release*'} }; anyOf { branch 'feature/*'; branch 'bugfix/*' } }
            steps {
                sh label: """ echo Change detected on feature or bugfix branch: -- ${env.GIT_BRANCH} --. """, returnStdout: true,
                   script: """ echo "Execute steps for ${env.GIT_BRANCH} branch here." """
                   //sh label: 'Deploying project tar file to ansible server.', script: """ scp "${env.PROJECT_NAME}.tar.gz" "cents@10.6.6.50:/opt/slamr/build/dev/apps/." """
                   run_ansible_update_playbook()
            }
        }
        stage('Deploy to integration & feature testing environments.') {
            when { branch 'develop'}
            steps {
                sh label: """ echo Change detected on feature or bugfix branch: -- ${env.GIT_BRANCH} --. """, returnStdout: true,
                   script: """ echo "Execute steps for ${env.GIT_BRANCH} branch here." """
                   //sh label: 'Deploying project tar file to ansible server.', script: """ scp "${env.PROJECT_NAME}.tar.gz" "cents@10.6.6.50:<DEPLOYMENT PATH TBD>" """
                   run_ansible_update_playbook()
            }
        }
        stage('Deploy to staging prep environment.') {
            when { branch 'master'}
            steps{
                sh label: """ echo Change detected on -- master -- branch. """, returnStdout: true,
                   script: """ echo "Execute steps for ${env.GIT_BRANCH} branch here." """
                   //sh label: 'Deploying project tar file to ansible server.', script: """ scp "${env.PROJECT_NAME}.tar.gz" "cents@10.6.6.50:<DEPLOYMENT PATH TBD>" """
                   run_ansible_update_playbook()
            }
        }
        stage('Deploy to release environment') {
            when { branch 'release/*'}
            steps{
                sh label: """ echo Change detected on */release/* branch. """, returnStdout: true,
                   script: """ echo "Execute steps for ${env.GIT_BRANCH} branch here." """
                   //sh label: 'Deploying project tar file to ansible server.', script: """ scp "${env.PROJECT_NAME}.tar.gz" "cents@10.6.6.50:<DEPLOYMENT PATH TBD>" """
                   run_ansible_update_playbook()
            }
        }
        stage('Deploy to release feature/bugfix environment.') {
            // Feature or bugfix branch created from a release branch must have "release" prefixed to the branch name
            when { anyOf { branch 'feature/release*'; branch 'bugfix/release*' } }
            steps{
                sh label: """ echo Change detected on a release feature or bugfix branch. """, returnStdout: true,
                   script: """ echo "Execute steps for ${env.GIT_BRANCH} branch here." """
                 //sh label: 'Deploying project tar file to ansible server.', script: """ scp "${env.PROJECT_NAME}.tar.gz" "cents@10.6.6.50:<DEPLOYMENT PATH TBD>" """
                 run_ansible_update_playbook()
            }
        }
    }

    post {
        always {
            junit 'pipeline/artifacts/test_results.xml'
            step([$class: 'CoberturaPublisher',
                          autoUpdateHealth: false,
                          autoUpdateStability: false,
                          coberturaReportFile: 'pipeline/artifacts/coverage/coverage.xml',
                          failUnhealthy: true,
                          failUnstable: true,
                          maxNumberOfBuilds: 0,
                          onlyStable: false,
                          sourceEncoding: 'ASCII',
                          zoomCoverageChart: false])
             sendSlackNotification(currentBuild.currentResult, getTestResultsForSlack())


        }
    }
}
/*
    Determines the name of the project based on the Bitbucket repository name.
*/
def get_project_name() {
    sh (label: '------Getting project name------',
        script: '''basename -s .git `git config --get remote.origin.url`''',
        returnStdout: true
    ).trim()
}
/*
    Creates a tar file of project contents needed to run on microservice on the target deployment server.
*/
def create_project_tar() {
    sh label: 'Creating project directory.', returnStdout: true,
        script: """ mkdir -p "$WORKSPACE/${env.PROJECT_NAME}" """
    sh label: 'Coping contents to project directory.', returnStdout: true,
        script: """ cp -rv vendor ${env.PROJECT_NAME}.gemspec lib Gemfile config bin "$WORKSPACE/${env.PROJECT_NAME}/." """
    sh label: 'Compressing project directory to a tar file.', returnStdout: true,
        script: """ tar -czf "${env.PROJECT_NAME}.tar.gz" "${env.PROJECT_NAME}" """
}
/*
    Executes the ansible playbook to update the service on the target deployment server.
*/
def run_ansible_update_playbook() {
    def ansible_update_command = ""
    def playbook_call = 'ansible-playbook -i /opt/slamr/slamr-devops-automation/inventory/hosts /opt/slamr/slamr-devops-automation/update_build_example_ruby.yml --limit '
    //Determine the hosts to run the playbook on based on the branch.
    if ((env.BRANCH_NAME.startsWith('feature/') || env.BRANCH_NAME.startsWith('bugfix/')) && !(env.BRANCH_NAME.startsWith('feature/release') || env.BRANCH_NAME.startsWith('bugfix/release'))){
        ansible_update_command = "${playbook_call}" + "'nextctrldev.cybercents.local'"
    } else if (env.BRANCH_NAME.startsWith('develop')){
        ansible_update_command = "${playbook_call}" + "'nextctrlfeat.cybercents.local','nextctrlintg.cybercents.local'"
    } else if (env.BRANCH_NAME.startsWith('master')){
       //TBD
    } else if (env.BRANCH_NAME.startsWith('release')){
       //TBD
    } else if (env.BRANCH_NAME.startsWith('feature/release') || env.BRANCH_NAME.startsWith('bugfix/release')){
       //TBD
    }
    echo "ANSIBLE UPDATE COMMAND: ${ansible_update_command}"
    sshPublisher(publishers:[sshPublisherDesc(
                 configName: 'ccansible',
                 transfers:[sshTransfer
                 (execCommand: ansible_update_command,
                 execTimeout: 120000 )], verbose: true)])
}
/*
    Gets test results info for slack notification.
*/
@NonCPS
def getTestResultsForSlack() {
    AbstractTestResultAction testResultAction =  currentBuild.rawBuild.getAction(AbstractTestResultAction.class)
    if (testResultAction != null) {
        def total = testResultAction.totalCount
        def failed = testResultAction.failCount
        def skipped = testResultAction.skipCount
        def passed = total - failed - skipped
        testResults = "Passed: ${passed}, Failed: ${failed} ${testResultAction.failureDiffString}, Skipped: ${skipped}"
    }
    return testResults
}
/*
    Gets the author of git commit.
*/
def getGitAuthor() {
    def commit = sh(returnStdout: true, script: 'git rev-parse HEAD')
    sh(returnStdout: true, script: "git --no-pager show -s --format='%an' ${commit}").trim()
}
/*
    Gets the git commit message.
*/
def getLastCommitMessage() {
    sh(returnStdout: true, script: 'git log -1 --pretty=%B').trim()
}
/*
    Sends slack notification to project channel including build status, git author, commit message, and test results.
*/
def sendSlackNotification(String buildResult, String testResults) {
    def teamDomain = 'cybercents'
    def username = 'jenkins'
    def jenkinsIcon = 'https://wiki.jenkins.io/download/attachments/2916393/logo.png'
    def channel = "#build_example_ruby"
    // Token for Slack Jenkins Bot
    def tokenCredentialId = '13cb488a-d365-4698-bc62-ffe8860e1d6f'
    def committerUserID = slackUserIdFromEmail('khyati.patel@bylight.com')
    def message = "Job: ${env.JOB_NAME}\n" +
                  "Git Commit Author: <@$committerUserID>\nLast Git Commit Message: ${getLastCommitMessage()}\n" +
                  "\nBuild Number: ${env.BUILD_NUMBER}\nBuild Status: ${buildResult}." +
                  "\nTest Results: ${testResults} (<${env.BUILD_URL}|Open>)"
    def color = '#000000'
    if ( buildResult == "SUCCESS" ) {
        color = 'good'
    }else if( buildResult == "FAILURE" ) {
        color = 'danger'
    }else if( buildResult == "UNSTABLE" ) {
        color = "warning"
    }else {
        color = 'danger'
    }
    slackSend color: color, botUser: true, notifyCommitters: true, teamDomain: teamDomain, username: username,
              channel: "$channel, $committerUserID", tokenCredentialId: tokenCredentialId, message: message
}
