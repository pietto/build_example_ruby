#!/usr/bin/env ruby

# Copyright © 2009-2018 by Metova Federal, LLC.  All rights reserved.
#
# SLAM-R Copyright © 2009-2018 by Metova Federal, LLC.  All rights reserved.
#
# Patent Issued, Metova Federal, LLC; Patent No.: US9,246,768, January 26, 2016 – Systems and Methods for a Simulated Network Attack Generator.
#
# Patent Issued, Metova Federal, LLC; Patent No.: US8,751,629, June 10, 2014 – Systems and Methods for Automated Building of a Simulated Network Environment.
#
# Patent Issued, Metova Federal, LLC; Patent No.: US8,532,970 B2, September 10, 2013 – Systems and Methods for Network Monitoring and Analysis of a Simulated Network.
#
# Additional Patents Pending 2009-2018 through the United States Patent and Trademark Office (USPTO).
#
# CYNTRS®, HOTSIM®, RGI®, VCCE®, SLAM-R®, and CyberCENTS® are registered trademarks of Metova Federal, LLC through the USPTO


#=================================================================================
# EXECUTE SCRIPT
#=================================================================================

require_relative '../lib/build_example_ruby.rb'

ber = BuildExampleRuby::Base.new
ber.run
