require 'bundler/setup'
require 'simplecov'
require 'simplecov-cobertura'

# RSpec.configure do |config|
#   # Enable flags like --only-failures and --next-failure
#   config.example_status_persistence_file_path = '.rspec_status'
#
#   # Disable RSpec exposing methods globally on `Module` and `main`
#   config.disable_monkey_patching!
#
#   config.expect_with :rspec do |c|
#     c.syntax = :expect
#   end
# end

SimpleCov.formatters =
    SimpleCov::Formatter::MultiFormatter.new([
                                                 SimpleCov.formatter = SimpleCov::Formatter::HTMLFormatter,
                                                 SimpleCov.formatter = SimpleCov::Formatter::CoberturaFormatter
                                             ])
# SimpleCov.minimum_coverage 80 # minimum coverage percentage expected; SimpleCov will return non-zero if coverage decreases by more than this threshold; Jenkins job will FAIL
SimpleCov.start do
  coverage_dir './pipeline/artifacts/coverage' # directory to store coverage html files
  add_filter '.rspec'  # use add_filter to exclude files from code coverage report such as tests, configs
  add_filter 'Gemfile'
  add_filter 'Gemfile.lock'
  add_filter 'spec/'
end
SimpleCov.at_exit do
    SimpleCov.result.format!  # default exit behavior; can be customized for need
end
