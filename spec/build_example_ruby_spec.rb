require_relative 'spec_helper'
require_relative '../lib/build_example_ruby.rb'

RSpec.describe BuildExampleRuby do
  it "has a version number" do
    expect(BuildExampleRuby::VERSION).not_to be nil
  end

  it "has a Base Class" do
    expect(BuildExampleRuby::Base.class).to eq(Class)
  end
  it "has a Hash base attribute" do
    expect(BuildExampleRuby::Base.new.base).to eq(Hash.new)
  end
end
